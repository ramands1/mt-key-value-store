import grpc
from concurrent import futures
import time

# import the generated classes
import keyvaluestore_pb2
import keyvaluestore_pb2_grpc

# import the original keyvaluestore.py
import keyvaluestore


# create a class to define the server functions, derived from
# keyvaluestore_pb2_grpc.KeyValueStoreServicer
class KeyValueStoreServicer(keyvaluestore_pb2_grpc.KeyValueStoreServicer):

    def SetVal(self, request, context):
        response = keyvaluestore_pb2.GetSetWatchReponse()
        response.message = keyvaluestore.set_value(request.key, request.value)
        return response

    def GetVal(self, request, context):
        response = keyvaluestore_pb2.GetSetWatchReponse()
        response.message = keyvaluestore.get_value(request.key)
        return response

    def WatchVal(self, request, context):
        response = self.GetVal(request, context)
        while True:
            new_response = self.GetVal(request, context)
            if new_response != response:
                response = new_response
                yield response


# create a gRPC server
server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

# use the generated function `add_KeyValueStoreServicer_to_server`
# to add the defined class to the server
keyvaluestore_pb2_grpc.add_KeyValueStoreServicer_to_server(
    KeyValueStoreServicer(), server
)

# listen on port 50051
server.add_insecure_port('[::]:50051')
server.start()

# since server.start() will not block,
# a sleep-loop is added to keep alive
try:
    while True:
        time.sleep(86400)
except KeyboardInterrupt:
    server.stop(0)
