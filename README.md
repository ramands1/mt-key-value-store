# MindTickle's Assignment - Key Value Store

## Problem Statement
Create a key value store server (like redis) and client (CLI). The client-server communication can take place through any protocol like TCP, HTTP etc. The client should be able to `get`, `set` and `watch` key value pairs.

## Implementation
* SQLite3 is being used as the key-value store. [SQLiteDict](https://github.com/RaRe-Technologies/sqlitedict) is being used as wrapper around SQLite3 to provice dict-like features. SQLite was chosen since it does not require any external dependencies and is extremely lightweight.
* gRPC (HTTP/2) is being used for the communication between the client cli and the server. gRPC was chosen since the `watch` command requires server streaming.
* [Click](https://click.palletsprojects.com/en/7.x/) is being used for creating the CLI functionality for Python.
* Docker/docker-compose is being used for the entire setup.

## Video demo of mt-cli
[Youtube Link (Unlisted Video)](https://youtu.be/drap7iT36UA)

## Getting Started
* Prerequisites - docker and docker-compose

* Clone the repo
```
git clone https://ramands1@bitbucket.org/ramands1/mt-key-value-store.git
cd mt-key-value-store
```

* Start the server and client containers
```
# Run the docker container. This will also install all the dependencies and start the server and client containers
docker-compose up
```

## Client Usage
```
# Get inside the first client container
docker exec -it mt-kv-client-1-container /bin/bash

# Run the help command to see the CLI usage and commands available
mt-cli --help

# Connect to the client. The default host is localhost and default port is 50051.
# To set the host and port explicitly, you can pass the --host and --port arguments.
mt-cli --host mt-kv-store-server --port 50051

# You will now be shown the REPL Terminal where you can execute the get, set, and watch commands
```

## Set Command
```
> set --help
> set user:1:first_name Yash
> set user:1:last_name Mehrotra
> set samplename Foo
> set sampleage 22
```

## Get Command
```
> get --help
> get user:1:first_name
> get sampleage
```

## Watch Command
```
# The watch command will listen for any changes made to a given key.
# This is being done using the gRPC Server Side streaming feature.

> watch --help
> watch samplename

# Open a new terminal session and get inside the second client container.

docker exec -it mt-kv-client-2-container /bin/bash
mt-cli --host mt-kv-store-server --port 50051
> set samplename Bar
```

Here we have used multiple containers for simplicty. The Client CLI can also be installed locally by running the `pip install .` command.

NOTE: We can also run the commands in the same line (non-REPL mode):
```
mt-cli --host mt-kv-store-server --port 50051 set user:1:first_name Yash

mt-cli --host mt-kv-store-server --port 50051 get user:1:first_name
```

## Files Used

* protos/keyvaluestore.proto - Protocol Buffer file. Service and format definitions. Used for gRPC.
* keyvaluestore.py - Python file containing the actual logic of `set`, `get`, and `watch` commands.
* keyvaluestore_pb2.py and keyvaluestore_pb2_grpc.py - Auto generated files for gRPC. These contain the messages and classes required by the client and server. No modifications are to be made in these files.
* mt.py - Python file containing the client (CLI) implementation code.
* server.py - Pythong file containing the server side implementation code.
* setup.py - Used to create and install packages using the pip command.

## Known Issues in the assignment.
* No unit tests written.
* Client and Servers are installed together in the same package. In a production env, separate packages will be better.
* The data is being stored on disk. For faster retrieval of data in prod environment, the data can be stored in-memory. SQLite3 can be used as an in-memory database as well.