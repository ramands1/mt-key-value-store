from sqlitedict import SqliteDict


def get_value(key):
    """Get the value of the key"""
    mydict = SqliteDict('./mtkvstore.sqlite', autocommit=True)
    if key in mydict:
        return mydict[key]
    else:
        return ""


def set_value(key, value):
    """Set the value of the key"""
    mydict = SqliteDict('./mtkvstore.sqlite', autocommit=True)
    try:
        mydict[key] = value
        return "OK"
    except Exception:
        return "Something went wrong"
