import click
import grpc
import keyvaluestore_pb2
import keyvaluestore_pb2_grpc

from click_shell import shell


class Config(object):
    def __init__(self):
        self.stub = ""


pass_config = click.make_pass_decorator(Config, ensure=True)


@shell(prompt='mt-cli > ')
@click.option('--host', default='0.0.0.0', help='Set Host. Default: localhost')
@click.option('--port', default=50051, help='Set Port. Default: 50051')
@pass_config
def cli(config, host, port):
    """Command Line Interface for MT's in-memory store."""
    connection_string = "{}:{}".format(host, port)
    channel = grpc.insecure_channel(connection_string)
    config.stub = keyvaluestore_pb2_grpc.KeyValueStoreStub(channel)
    try:
        # Check if the grpc server is ready to establish connection.
        grpc.channel_ready_future(channel).result(timeout=5)
    except Exception:
        raise click.UsageError(
            "Connection refused - {}".format(connection_string),
            ctx=None
        )


@cli.command()
@click.argument('key')
@click.argument('value')
@pass_config
def set(config, key, value):
    """Create or update a key-value pair in the memory store"""
    request = keyvaluestore_pb2.SetRequest(key=key, value=value)
    response = config.stub.SetVal(request)
    click.echo(response.message)


@cli.command()
@click.argument('key')
@pass_config
def get(config, key):
    """Get the value of a key from the memory store"""
    request = keyvaluestore_pb2.GetWatchRequest(key=key)
    response = config.stub.GetVal(request)
    click.echo(response.message)


@cli.command()
@click.argument('key')
@pass_config
def watch(config, key):
    """Detect changes made to a key in real-time"""
    request = keyvaluestore_pb2.GetWatchRequest(key=key)
    response = config.stub.WatchVal(request)
    for r in response:
        click.echo("Updated Value: {}".format(r.message))


if __name__ == '__main__':
    cli()
