from setuptools import setup

setup(
    name='mtcli',
    version='1.0.0',
    py_modules=['mt'],
    install_requires=[
        'click_shell==2.0',
        'grpcio==1.13.0',
        'grpcio-tools==1.13.0',
        'sqlitedict==1.7.0',
        'requests==2.24.0'
    ],
    entry_points='''
        [console_scripts]
        mt-cli=mt:cli
    ''',
)
